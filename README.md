
# Personal Website

This repository contains the source code for my personal website. The website is built using Zola, a fast static site generator in a single binary with everything built-in.

## Website Functionality

The website includes the following features:

- Responsive design for compatibility with various devices and screen sizes
- Accessibility enhancements for better navigation and readability
- SEO-friendly URLs and metadata

## GitLab Workflow

`.gitlab-ci.yml` is provided to facilitate Continuous Integration/Continuous Deployment (CI/CD). This file contains the definition for the pipeline responsible for testing, building, and deploying the website on every push to the repository.

## Hosting and Deployment

The site is automatically deployed to Vercel, which provides hosting and serverless backend services for static sites.

- Personal Website URL: [Link here](https://individual-project-1-ivory.vercel.app/)


## Documentation

Detailed documentation is available in the `docs` folder. This includes:

- Setup instructions
- Development guide
- Build and deployment process

## Demo Video

A demo video is available at [link to video](https://www.youtube.com/watch?v=dP5K0op2wWw). This video provides a walkthrough of the website's features, the GitLab pipeline, and the deployment process.

## Quick Start

To get started with this project, clone the repository and install the dependencies.

```bash
git clone https://gitlab.com/username/personal-website.git
cd personal-website
# Install Zola if not already installed
# https://www.getzola.org/documentation/getting-started/installation/
zola serve
```

Open `http://localhost:1111` in your browser to view the site.


