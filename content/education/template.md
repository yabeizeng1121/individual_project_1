+++
title = "Duke University"

[extra]
image = "https://1000logos.net/wp-content/uploads/2017/11/Duke-University-Logo.png"
link = "https://duke.edu"
+++

Master Degree (2023-2025):
  - Interdisciplinary Data Science

