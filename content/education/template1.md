+++
title = "University of California - Santa Barbara"

[extra]
image = "https://brand.ucsb.edu/sites/default/files/images/pages/Visual-Identity/Marks/Seal/2-color-seal.gif"
link = "https://www.ucsb.edu"
+++

Bachelor Degree (2019-2023):
  - Major: Statistics and Data Science
  - Minor: Earth Science
