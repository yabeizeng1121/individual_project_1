+++
title = "Skills"
template = "skills.html"
page_template = "skills.html"

[extra]
author = "Yabei Zeng"
image = "https://cdn.elearningindustry.com/wp-content/uploads/2019/10/7-Benefits-That-Highlight-The-Importance-Of-Soft-Skills-In-The-Workplace.png"

lan = [
{ lang = "Python", expr = "4", image = "https://1000logos.net/wp-content/uploads/2020/08/Python-Logo.png", link = ""}, 
{ lang = "R", expr = "4", image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdajmhf-kXuFX439ovCRVpa3LBND7E6MZLUfw4h7yrkQ&s", link = ""}, 
{ lang = "SQL", expr = "3", image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfchmi7s5YYWYiJ8HiSmQ0yIW5oAFxplfwHuSPa2HBnQ&s", link = "https://www.example.com"}, 
{ lang = "C++", expr = "2", image = "https://w7.pngwing.com/pngs/46/626/png-transparent-c-logo-the-c-programming-language-computer-icons-computer-programming-source-code-programming-miscellaneous-template-blue.png", link = ""}, 
]

tools = [
{ tool = "AWS", expr = "2", image = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/2560px-Amazon_Web_Services_Logo.svg.png", link = ""},
{ tool = "Azure", expr = "2", image = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Microsoft_Azure_Logo.svg/2560px-Microsoft_Azure_Logo.svg.png", link = ""},
{ tool = "Git", expr = "4", image = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1200px-Git-logo.svg.png", link = ""},
{ tool = "Microsoft Offic", expr = "4", image = "https://download.logo.wine/logo/Microsoft_Office_2007/Microsoft_Office_2007-Logo.wine.png", link = ""},
]
+++
