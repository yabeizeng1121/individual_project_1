+++
title = "Steam Review Analyzer"

[extra]
image = "https://static.vecteezy.com/system/resources/previews/020/975/557/original/steam-logo-steam-icon-transparent-free-png.png"
link = "https://github.com/nogibjj/Steam_Review__Analyzer"
technologies = ["NLP", "Data visualization", "Data scraping", "ETL pipeline"]
+++
Docker contained Microservice, that takes user specification, and displays a Dashboard filled with game analysis based off their Steam Reviews. 

