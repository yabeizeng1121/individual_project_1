+++
title = "Text Summarizer"

[extra]
image = "https://thumbs.dreamstime.com/b/summary-icon-white-background-simple-element-illustration-technology-concept-sign-symbol-design-141335593.jpg"
link = "https://github.com/nogibjj/Individual_Project_4_Yabei"
technologies = ["Dashboard Design", "Flask", "Azure App", "Docker"]
+++

Designed an engaging web interface, enhancing user interaction and accessibility to text summarization services

