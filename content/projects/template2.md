+++
title = "Extreme Weather Research"

[extra]
image = "https://static.vecteezy.com/system/resources/thumbnails/005/387/410/small/summer-abstract-weather-logo-free-vector.jpg"
link = "https://github.com/yabeizeng1121/Climate_project"
technologies = ["Machine Learning", "Data Analysis", "LSTM", "Climate Modeling"]
+++

Utilized Variable Correlation, Linear Regression, and VIF for climate data analysis, identifying key temperature influencers.

