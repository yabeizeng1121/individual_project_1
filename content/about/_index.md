+++
title = "About"
template = "about.html"

[extra]
author = "Yabei Zeng"
image = "https://media.licdn.com/dms/image/D4E03AQHO32A_INWRAg/profile-displayphoto-shrink_800_800/0/1695418079805?e=1715212800&v=beta&t=JIGGGiaz9Nl4083tfdbkZ6srCaqUyH0BLgjwNGVgbZk"
+++

Hi, I am Yabei Zeng, a master student majoring in Interdisciplinary Data Science at Duke University